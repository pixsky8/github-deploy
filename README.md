# Github Deployment

Basic Sinatra server to run a script when a push is made on the master branch
of a Github repository

## Dependencies

- Ruby
- Sinatra

## Running

`ruby app.rb -e stable -p <port>` will start the server.

Add your IP address as payload URL followed by the port and `/event_handler`

`http[s]://<ip/domain>:<port>/event_handler`

Example: `203.0.113.42:4567/event_handler`

## Config

To specify your repository you must change the `config.yaml` file.

- `repo-name` represents the full name of the repository
(usually `<username>/<repository_name>`)
- `path` represents the path of the local repository, where the repo is stored
on your machine
- `script` is the path, from the root of the repo, to the script that will run
when a push is made
