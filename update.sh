#! /bin/bash

cd "$(dirname "$0")"

git pull
killall app.rb
nohup ruby app.rb &

cd -
