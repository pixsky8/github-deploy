require 'sinatra'
require 'json'

require './update.rb'

get '/' do
	"Hello world!"
end

get '/hello/:name' do
	"Hello #{params['name']}!"
end

post '/event_handler' do
	request.body.rewind
	@payload = JSON.parse(request.body.read)

	case request.env['HTTP_X_GITHUB_EVENT']
	when "push"
		ref = @payload["ref"]
		if !ref.empty?
			branch = ref.split('/').last
			if branch == @payload["repository"]["master_branch"]
				update(@payload["repository"]["full_name"])
			end
		end
	end

	"Well, it worked!"
end
